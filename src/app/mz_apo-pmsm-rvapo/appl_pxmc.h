/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  appl_pxmc.h - SPI connected motor control board specific
                extensions

  (C) 2001-2021 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2021 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _APPL_PXMC_H_
#define _APPL_PXMC_H_

#include <stdint.h>
#include <pxmc.h>

struct z3pmdrv1_wmcc_state_t;
typedef struct pxmc_z3pmdrv1_wmcc_state_t {
  pxmc_state_t base;
  struct z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst;
  uint32_t steps_pos_prev;
  uint32_t cur_d_cum_prev;
  uint32_t cur_q_cum_prev;
  int32_t  cur_d_err_sum;
  int32_t  cur_q_err_sum;
  short    cur_d_p;
  short    cur_d_i;
  short    cur_q_p;
  short    cur_q_i;
  short    cur_hold;
} pxmc_z3pmdrv1_wmcc_state_t;

#define pxmc_z3pmdrv1_wmcc_state_offs(_fld) \
                (((size_t)&((pxmc_z3pmdrv1_wmcc_state_t *)0L)->_fld) - \
                 ((size_t)&((pxmc_z3pmdrv1_wmcc_state_t *)0L)->base))

/**
 * @mcs:     A state struct from the pxmc library
 * @return:  The pxmc_z3pmdrv1_wmcc_state_t struct in which mcs is .base.
 */
static inline
pxmc_z3pmdrv1_wmcc_state_t *pxmc_state2z3pmdrv1_wmcc_state(pxmc_state_t *mcs)
{
  pxmc_z3pmdrv1_wmcc_state_t *mcsz3d1;
#ifdef UL_CONTAINEROF
  mcsz3d1 = UL_CONTAINEROF(mcs, pxmc_z3pmdrv1_wmcc_state_t, base);
#else  /*UL_CONTAINEROF*/
  mcsz3d1 = (pxmc_z3pmdrv1_wmcc_state_t *)((char *)mcs - __builtin_offsetof(pxmc_z3pmdrv1_wmcc_state_t, base));
#endif /*UL_CONTAINEROF*/
  return mcsz3d1;
}

extern int appl_errstop_mode;
extern int appl_idlerel_time;
//some half-understood hack copied from lx-rocon
extern uint32_t pxmc_rocon_rx_err_level;
extern uint32_t pxmc_rocon_mcc_stuck;
int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits);

int pxmc_z3pmdrv1_wmcc_pwm_direct_wr(unsigned chan, unsigned pwm, int en);

#endif /*_APPL_PXMC_H_*/
