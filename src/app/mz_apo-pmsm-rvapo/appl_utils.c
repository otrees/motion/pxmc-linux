#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <sched.h>
#include <unistd.h>
#include <sys/mman.h>  /* this provides mlockall() */
#include <pthread.h>
#include <signal.h>
#include <time.h>

#include "appl_utils.h"

int appl_base_task_prio;

int create_rt_task(pthread_t *thread, int prio, void *(*start_routine) (void *), void *arg)
{
  int ret ;

  pthread_attr_t attr;
  struct sched_param schparam;

  if (pthread_attr_init(&attr) != 0) {
    fprintf(stderr, "pthread_attr_init failed\n");
    return -1;
  }

  if (pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0) {
    fprintf(stderr, "pthread_attr_setinheritsched failed\n");
    return -1;
  }

  if (pthread_attr_setschedpolicy(&attr, SCHED_FIFO) != 0) {
    fprintf(stderr, "pthread_attr_setschedpolicy SCHED_FIFO failed\n");
    return -1;
  }

  schparam.sched_priority = prio;

  if (pthread_attr_setschedparam(&attr, &schparam) != 0) {
    fprintf(stderr, "pthread_attr_setschedparam failed\n");
    return -1;
  }

  ret = pthread_create(thread, &attr, start_routine, arg);

  pthread_attr_destroy(&attr);

  return ret;
}

int sample_period_setup(sample_period_t *sper, unsigned long pernsec)
{
  sper->period_nsec = pernsec;
  clock_gettime(CLOCK_MONOTONIC, &sper->period_time);
  return 0;
}

int sample_period_modify(sample_period_t *sper, unsigned long pernsec)
{
  sper->period_nsec = pernsec;
  return 0;
}

unsigned long sample_period_get(sample_period_t *sper)
{
  return sper->period_nsec;
}

int sample_period_wait_next(sample_period_t *sper)
{
  sper->period_time.tv_nsec += sper->period_nsec;
  if (sper->period_time.tv_nsec > 1000*1000*1000) {
    sper->period_time.tv_nsec -= 1000*1000*1000;
    sper->period_time.tv_sec += 1;
  }
  return clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &sper->period_time, NULL);
}

void appl_sig_handler(int sig)
{
    appl_stop();
    exit(1);
}

int appl_setup_environment(const char *argv0)
{
  struct sigaction sigact;
  int fifo_min_prio = sched_get_priority_min(SCHED_FIFO);
  int fifo_max_prio = sched_get_priority_max(SCHED_FIFO);

  appl_base_task_prio = fifo_max_prio - 20;
  if (appl_base_task_prio < fifo_min_prio)
    appl_base_task_prio = fifo_min_prio;

#if !defined(__rtems__)
  if (mlockall(MCL_FUTURE | MCL_CURRENT) < 0) {
    fprintf(stderr, "%s: mlockall failed - cannot lock application in memory\n", argv0);
    exit(1);
  }
#endif /*__rtems__*/

  atexit(appl_stop);

  memset(&sigact, 0, sizeof(sigact));
  sigact.sa_handler = appl_sig_handler;
  sigaction(SIGINT, &sigact, NULL);
  sigaction(SIGTERM, &sigact, NULL);

  return 0;
}
