#ifndef _APPL_UTILS_H
#define _APPL_UTILS_H

#include <pthread.h>
#include <time.h>

extern int appl_base_task_prio;

typedef struct sample_period_t {
  unsigned long period_nsec;
  struct timespec period_time;
} sample_period_t;

int sample_period_setup(sample_period_t *sper, unsigned long pernsec);

int sample_period_modify(sample_period_t *sper, unsigned long pernsec);

unsigned long sample_period_get(sample_period_t *sper);

int sample_period_wait_next(sample_period_t *sper);

int create_rt_task(pthread_t *thread, int prio, void *(*start_routine) (void *), void *arg);

extern void appl_stop(void);

int appl_setup_environment(const char *argv0);

#endif /*_APPL_UTILS_H*/