/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  appl_pxmc.c - SPI connected motor control board specific
                extensionsposition

  Copyright (C) 2001-2021 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2021 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_internal.h>
#include <pxmc_inp_common.h>
#include <pxmc_gen_info.h>
#include <pxmc_dq_trans.h>
#include <pxmc_sin_fixed.h>
#include <stdlib.h>
#include <string.h>

#if !defined(clzl) && !defined(HAVE_CLZL)
#define clzl __builtin_clzl
#endif

#include "pxmcc_types.h"
#include "appl_defs.h"
#include "appl_pxmc.h"
#include "appl_utils.h"
#include "zynq_pmsm_rvapo_mc.h"
#include "out_pxmc.h"
#include "inp_pxmc.h"

//expands the default states defined in pxmc.h
#define PXMC_AXIS_MODE_BLDC_PXMCC (PXMC_AXIS_MODE_BLDC + 1)             /*6*///used
#define PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC (PXMC_AXIS_MODE_BLDC + 2) /*7*///unused
#define PXMC_AXIS_MODE_STEPPER_PXMCC (PXMC_AXIS_MODE_BLDC + 3)          /*8*///unused
#define PXMC_AXIS_MODE_BLDC_DQ (PXMC_AXIS_MODE_BLDC + 4)                /*9*///extended state
#define PXMC_AXIS_MODE_DC_CURCTRL (PXMC_AXIS_MODE_BLDC + 5)             /*10*///unused

pthread_t pxmc_base_thread_id;

//int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,unsigned long index_irc, int diff2err);

#define PXML_MAIN_CNT 1
#define PXMC_WITH_PT_ZIC 1  //used by pxmc_axis_pt4mode

unsigned pxmc_z3pmdrv1_wmcc_pwm_magnitude = PXMC_Z3PMDRV1_WMCC_PWM_CYCLE;
/**
 *This can be used to externally stop axes. 
*/
int appl_errstop_mode = 0;
uint32_t pxmc_rocon_rx_err_level;
uint32_t pxmc_rocon_mcc_stuck;

/**
 * This should set actual position to hw.
 * According to pxmc_base.pxmc_axis_set_pos, this should mean the irc position in pxms_ap, pxms_rp 
*/
int pxmc_inp_z3pmdrv1_wmcc_ap2hw(struct pxmc_state *mcs)
{
  pxmc_z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs);
  /*int chan=mcs->pxms_inp_info;*/
  uint32_t irc_val=pxmcc_read_irc_injectable(mcs);//*((uint32_t*)mcsrc->z3pmdrv1_wmccst->pxmcc_ctrl_struct->inp_info);
  long pos_diff;

  //if (z3pmdrv1_wmcc_irc_read(mcsrc->z3pmdrv1_wmccst, &irc_val) < 0)return -1;
  if(irc_val<0)return -1;

  pos_diff = mcs->pxms_ap - (irc_val << PXMC_SUBDIV(mcs));

  irc_val = pos_diff >> PXMC_SUBDIV(mcs);

  /* Adjust phase table alignemt to modified IRC readout  */
  mcs->pxms_ptofs += irc_val - mcsrc->z3pmdrv1_wmccst->pos_offset;//TODO: why is this sometimes increment and other times decrement?

  mcsrc->z3pmdrv1_wmccst->pos_offset = irc_val;
  return 0;
}

/**
 * old: pxmc_z3pmdrv1_wmcc_pwm_dc_out - DC motor CW and CCW PWM output
 * @mcs:  Motion controller state information

int pxmc_z3pmdrv1_wmcc_pwm_dc_out(pxmc_state_t *mcs)
{
  pxmc_z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs);
   int chan = mcs->pxms_out_info; 
  int ene = mcs->pxms_ene;

  z3pmdrv1_wmcc_bidirpwm_set(mcsrc->z3pmdrv1_wmccst, ene);

  return 0;
}*/
/*now has an implementation taken from lx-rocon in out_pxmc.c
int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,
                          unsigned long index_irc, int diff2err)
{
  return -1;
}*/

/**
 * pxmc_dummy_con - Dummy controller for synchronous BLDC/PMSM/steper drive
 * @mcs:        Motion controller state information
 */
int pxmc_dummy_con(pxmc_state_t *mcs)
{
  return 0;
}

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return NULL;
}

z3pmdrv1_wmcc_state_t z3pmdrv1_wmcc_state0 = {
    .hw_registers = NULL,
    .pxmcc_ctrl_struct=NULL
};

pxmc_z3pmdrv1_wmcc_state_t mcs0 =
    {
        .base = {
            .pxms_flg =
                PXMS_ENI_m,
            .pxms_do_inp =
                pxmc_inp_z3pmdrv1_wmcc_inp,
            .pxms_do_con =
                pxmc_pid_con /*pxmc_dummy_con*/,
            .pxms_do_out =
                pxmc_pxmcc_pwm3ph_out,//pxmc_z3pmdrv1_wmcc_pwm_dc_out,
            .pxms_do_deb = 0,//debug
            .pxms_do_gen = 0,//and position generator are used neither in mz_apo-2dc-test1 nor in lx-rocon
            .pxms_do_ap2hw =
                pxmc_inp_z3pmdrv1_wmcc_ap2hw,
            .pxms_ap = 0,
            .pxms_as = 0,
            .pxms_rp = 55 * 256,
            .pxms_rs = 0,
#ifndef PXMC_WITH_FIXED_SUBDIV
            .pxms_subdiv = 8,
#endif /*PXMC_WITH_FIXED_SUBDIV*/
            .pxms_md = 800 << 8,
            .pxms_ms = 500,
            .pxms_ma = 10,
            .pxms_inp_info = 0,
            .pxms_out_info = 0,
            .pxms_ene = 0,
            .pxms_erc = 0,
            .pxms_p = 40,
            .pxms_i = 10,
            .pxms_d = 100,
            .pxms_s1 = 0,
            .pxms_s2 = 0,
            .pxms_me = 0x7e00 /*0x7fff*/,
            .pxms_cfg =
                PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
                PXMS_CFG_HRI_m * 0 | PXMS_CFG_HDIR_m * 0 |
                PXMS_CFG_I2PT_m * 0 | 0x2,

            .pxms_ptper = 1,
            .pxms_ptirc = 1000,
            .pxms_ptmark = 1180,
            /*.pxms_ptamp = 0x7fff,*/

            .pxms_hal = 0x40,
        },
        .z3pmdrv1_wmccst = &z3pmdrv1_wmcc_state0,
};

pxmc_state_t *pxmc_main_arr[PXML_MAIN_CNT] = {&mcs0.base};
/**
 *pxmc grabs this list using extern.
 *Then pxmc_for_each_mcs macro is used to iterate the pxmc_z3pmdrv1_wmcc_state_t.base pointers to pxmc_state_t structures
 *from the pxmc library, and pointer sorcery in pxmc_state2z3pmdrv1_wmcc_state is used to get the hardware-specific pxmc_z3pmdrv1_wmcc_state_t
 *that contains it.
 *C OOP at its finest.
*/
pxmc_state_list_t pxmc_main_list =
    {
        .pxml_arr = pxmc_main_arr,
        .pxml_cnt = 0};

static inline void pxmc_sfi_input(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENI_m - check if input (IRC) update is enabled */
    if (mcs->pxms_flg & PXMS_ENI_m)
    {
      pxmc_call(mcs, mcs->pxms_do_inp);
    }
  }
}
/*
* this is the actual motor control function: call to regulator and then output
*/
static inline void pxmc_sfi_controller_and_output(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENR_m - check if controller is enabled */
    if (mcs->pxms_flg & PXMS_ENR_m || mcs->pxms_flg & PXMS_ENO_m)
    {

      /* If output only is enabled, we skip the controller */
      if (mcs->pxms_flg & PXMS_ENR_m)
      {

        pxmc_call(mcs, mcs->pxms_do_con);

        /* PXMS_ERR_m - if axis in error state */
        if (mcs->pxms_flg & PXMS_ERR_m)
          mcs->pxms_ene = 0;
      }

      /* for bushless motors, it is necessary to call do_out
        even if the controller is not enabled and PWM should be provided. */
      pxmc_call(mcs, mcs->pxms_do_out);
    }
  }
}

static inline void pxmc_sfi_generator(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENG_m - check if requested value (position) generator is enabled */
    if (mcs->pxms_flg & PXMS_ENG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_gen);
    }
  }
}
/*
* Calls debug function on axes where enabled. Could be an inlined oneliner.
*/
static inline void pxmc_sfi_dbg(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    if (mcs->pxms_flg & PXMS_DBG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_deb);
    }
  }
}
/**
 *Actual work of the motor control thread launched by pxmc_initialise. 
 *It calls wrappers for callbacks in pxmc_z3pmdrv1_wmcc_state_t, which check enable flags and the call the functions  
*/
void pxmc_samplig_period(void)
{
  pxmcc_poke_watchdog();  

  pxmc_sfi_input();//wrapper for pxms_do_inp (only if enabled)
  pxmc_sfi_controller_and_output();//wrapper for pxms_do_con and pxms_do_out
  pxmc_sfi_generator();//wrapper for requested position generator pxms_do_gen
  pxmc_sfi_dbg();//wrapper for debug function pxms_do_deb

  do_pxmc_coordmv();//call to pxmc library
}

sample_period_t pxmc_sample_period;

/*
*Function for the motor control thread launched by pxmc_initialise.
*/
void *pxmc_base_thread(void *arg)
{
  /* Set initial sampling frequnecy to 1 kHz (1000000 nsec) */
  sample_period_setup(&pxmc_sample_period, 1000 * 1000);

  do
  {
    pxmc_samplig_period();
    sample_period_wait_next(&pxmc_sample_period);
  } while (1);
}

#ifdef WITH_SFI_SEL
/**
 * pxmc_get_sfi_hz - Reads sampling frequency of axis
 * @mcs:        Motion controller state information
 */
long pxmc_get_sfi_hz(pxmc_state_t *mcs)
{
  unsigned long nsec;
  unsigned long nsec_in_sec = 1000 * 1000 * 1000;
  nsec = sample_period_get(&pxmc_sample_period);
  return  (nsec_in_sec + nsec / 2) / nsec;
}

/**
 * pxmc_sfi_sel - Setting sampling frequency of PXMC subsystem (TPU_TGR4A)
 * @sfi_hz:     Requested sampling frequency in Hz
 *
 * Function returns newly selected sampling frequency
 * after rounding or -1 if there is problem to set
 * requested frequency.
 */
long pxmc_sfi_sel(long sfi_hz)
{
  unsigned long nsec_in_sec = 1000 * 1000 * 1000;
  unsigned long nsec;

  if((sfi_hz < 500) || (sfi_hz > 5 * 1000))
    return -1;

  nsec = (nsec_in_sec + sfi_hz / 2) / sfi_hz;
  sample_period_modify(&pxmc_sample_period, nsec);
  return  (nsec_in_sec + nsec / 2) / nsec;
}
#endif /* WITH_SFI_SEL */

/**
 *pxmc library expects this to be implemented. Purpose unknown. 
*/
int pxmc_clear_power_stop(void)
{
  return 0;
}
/**
 * Checks state, and stops (some) axes if appl_errstop_mode is modified (externally, appears to be used only in lx-rocon).
 * @pbusy_bits:   Bitfield signalising busy channels/axes
 * @perror_bits:  Bitfield of channels/axes that have an error
 * @return:       Gathers all flags.
 */
int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits)
{
  unsigned short flg;
  unsigned short chan;
  unsigned long busy_bits = 0;
  unsigned long error_bits = 0;
  pxmc_state_t *mcs;
  flg = 0;
  pxmc_for_each_mcs(chan, mcs)
  {
    if (mcs)
    {
      flg |= mcs->pxms_flg;
      if (mcs->pxms_flg & PXMS_BSY_m)
        busy_bits |= 1 << chan;
      if (mcs->pxms_flg & PXMS_ERR_m)
        error_bits |= 1 << chan;
    }
  }
  if (appl_errstop_mode)
  {
    if ((flg & PXMS_ENG_m) && (flg & PXMS_ERR_m))
    { // if requested position generator enabled anywhere, and there is any error
      pxmc_for_each_mcs(chan, mcs)
      {
        if (mcs && (mcs->pxms_flg & PXMS_ENG_m))
        {                    // then stop axes with the generator enabled. TODO what is the generator?
          pxmc_stop(mcs, 0); // call to pxmc for smooth stop
        }
      }
    }
  }

  if (pbusy_bits != NULL)
    *pbusy_bits = busy_bits;
  if (perror_bits != NULL)
    *perror_bits = error_bits;

  return flg;
}

/**
 * Taken straight from lx-rocon, but support for dc and stepper removed.
*/
int pxmc_axis_pt4mode(pxmc_state_t *mcs, int mode){
 // static const typeof(*mcs->pxms_ptptr1) dummy0 = 0;
  int res = 0;

  if (mode == PXMC_AXIS_MODE_NOCHANGE)
    mode = pxmc_axis_rdmode(mcs);
  if (mode < 0)
    return -1;

  switch (mode){
    case PXMC_AXIS_MODE_BLDC:
    case PXMC_AXIS_MODE_BLDC_PXMCC:
    case PXMC_AXIS_MODE_BLDC_DQ:
      /* res = pxmc_init_ptable(mcs, PXMC_PTPROF_SIN3FUP); */
  #ifndef PXMC_WITH_PT_ZIC    //these are all pxmc library functions
      res = pxmc_ptable_set_profile(mcs, &pxmc_ptprofile_sin3phup, 0, 0);
  #else  /*PXMC_WITH_PT_ZIC*/
      res = pxmc_ptable_set_profile(mcs, &pxmc_ptprofile_sin3phup_zic, 0, 0);
  #endif /*PXMC_WITH_PT_ZIC*/
      break;
    default:
      return -1;
    }
  mcs->pxms_ptvang = pxmc_ptvang_deg2irc(mcs, 90);//call to pxmc library  
  return res;  
  }
/*
 * Determines operation mode, needs to mirror the switch-case in pxmc_axis_mode.
 * TODO: PXMC_AXIS_MODE_BLDC (no pxmcc) not supported
 */
int pxmc_axis_rdmode(pxmc_state_t *mcs)
{
//  if (mcs->pxms_do_out == pxmc_rocon_pwm3ph_out)
//    return PXMC_AXIS_MODE_BLDC;no coprocessor
  if (mcs->pxms_do_out == pxmc_pxmcc_pwm3ph_out)
    return PXMC_AXIS_MODE_BLDC_PXMCC;
#ifdef PXMC_WITH_EXTENDED_STATE
  if (mcs->pxms_do_out == pxmc_pxmcc_pwm3ph_dq_out)
    return PXMC_AXIS_MODE_BLDC_DQ;
#endif /*PXMC_WITH_EXTENDED_STATE*/
  return -1;
}

/**
 * pxmc_axis_mode - Sets axis mode.[extern API]
 * @mcs:        Motion controller state information
 * @mode:       0 .. previous mode, 1 .. stepper motor mode,
 *              2 .. stepper motor with IRC feedback and PWM ,
 *              3 .. stepper motor with PWM control
 *              4 .. DC motor with IRC feedback and PWM
 *
 */
int pxmc_axis_mode(pxmc_state_t *mcs, int mode)
{
  int res = 0;
  int prev_mode;

  pxmc_axis_release(mcs);
  pxmc_clear_flag(mcs, PXMS_ENI_b);
  pxmc_clear_flag(mcs, PXMS_ENO_b);
  /*TODO Clear possible stall index flags from hardware */

  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);

  prev_mode = pxmc_axis_rdmode(mcs); // this gets mode based on pxms_do_out callback

  if (mode == PXMC_AXIS_MODE_NOCHANGE)
    mode = prev_mode;
  if (mode < 0)
    return -1;
  if (!mode) // PXMC_AXIS_MODE_NOCHANGE is 0, so if prev_mode was also 0, default to DC motor
    mode = PXMC_AXIS_MODE_DC;

  // start lx-rocon copy
  if ((prev_mode == PXMC_AXIS_MODE_BLDC_PXMCC) ||
      (prev_mode == PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC) ||
      (prev_mode == PXMC_AXIS_MODE_STEPPER_PXMCC) ||
      (prev_mode == PXMC_AXIS_MODE_BLDC_DQ))
    pxmcc_axis_setup(mcs, PXMCC_MODE_IDLE); // this is a function that initialises coprocessor, using structs from pxmcc_types.h

  res = pxmc_axis_pt4mode(mcs, mode); // this looks like initialising the required mode in the pxmc library
  if (res < 0)
    return -1;
  // end lx-rocon copy
  // here lx-rocon had some special cases on 2764-2771

  switch (mode)
  {                         // gutted the lx-rocon switch to include only BLDC, therefore pxmc_rocon_pwm3ph_out
  case PXMC_AXIS_MODE_BLDC: // no coprocessor; may or may not be supported
 //   mcs->pxms_do_out = pxmc_rocon_pwm3ph_out;
    break;
  case PXMC_AXIS_MODE_BLDC_PXMCC:
#ifdef PXMC_WITH_EXTENDED_STATE // TODO: extended state for complex DQ control, should it be defined?
  case PXMC_AXIS_MODE_BLDC_DQ:
#endif                                              /*PXMC_WITH_EXTENDED_STATE*/
    if (pxmcc_axis_setup(mcs, PXMCC_MODE_BLDC) < 0) // coprocessor setup
      return -1;
    pxmcc_axis_enable(mcs, 1);
#ifdef PXMC_WITH_EXTENDED_STATE
    if (mode == PXMC_AXIS_MODE_BLDC_DQ)
    {
      mcs->pxms_do_out = pxmc_pxmcc_pwm3ph_dq_out;
      mcs->pxms_do_inp = pxmc_pxmcc_cur_acquire_and_irc_inp; // complex control, replace pxmc_inp_z3pmdrv1_wmcc_inp
    }                                                   //in lx-rocon it appears to call the original function 
    else
#endif /*PXMC_WITH_EXTENDED_STATE*/
    {
      mcs->pxms_do_out = pxmc_pxmcc_pwm3ph_out;
    }
    break;
  default:
    return -1;
  }

  /*TODO Clear possible stall index flags from hardware */
  /*lx-rocon: Clear possible stall index flags from hardware */
  //pxmc_inp_rocon_is_index_edge(mcs, 1);//Apparently pmsm+rvapo do not have such flags. TODO: Find out for sure.
  /* Request new phases alignment for changed parameters */
  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);
  pxmc_set_flag(mcs, PXMS_ENI_b);
  return res;
}
/*
Terminate motor control.
Called twice, in mz_apo_pmsm_rvapo_hwplugin.c and in app_main.c, both times from appl_stop
appl_stop also speaks of stopping fpga clock but does not do so; maybe do it here?
In fact this is boilerplate wrapper for pxmc call.
*/
int pxmc_done(void)
{
  int var;
  pxmc_state_t *mcs;

  if (!pxmc_main_list.pxml_cnt)
    return 0; // this condition is redundant

  pxmc_for_each_mcs(var, mcs)
  {
    pxmc_axis_release(mcs); // external call to pxmc library
  }

  pxmc_main_list.pxml_cnt = 0;
  __memory_barrier();

  return 0;
}
/**
 *This function initialises motor description structure mcs0 and starts the motor control thread.
 */
int pxmc_initialize(void)
{
  pxmc_main_list.pxml_cnt = 0;
  pxmc_dbg_hist = NULL;

  if (z3pmdrv1_wmcc_init(mcs0.z3pmdrv1_wmccst) < 0)
    return -1;

  __memory_barrier();
  pxmc_main_list.pxml_cnt = PXML_MAIN_CNT;

  if (create_rt_task(&pxmc_base_thread_id, 60, pxmc_base_thread, NULL) < 0)
    return -1;

  return 0;
}
