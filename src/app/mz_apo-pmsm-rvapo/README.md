This program uses the pxmc library to control a PMSM motor.
The algorithm is divided in two parts: a pid controller running on ARM+Linux and a commutator program on the [rvapo](https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-vhdl) softcore processor, both on the [mzapo board](https://cw.fel.cvut.cz/wiki/courses/b35apo/documentation/mz_apo/start).

The recommended way to run this program is through the [rvapo-apps](https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-apps/-/tree/master/apps/rvapo-pmsm?ref_type=heads) repository, which links both this and the rvapo repositories into a stable system.

Therefore this repository should only be accesed for development and debugging.