/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  mz_apo_2dc_hwplugin.c - exporting of PXMC functionality as
            hardware  plugin pro DINFO based controllers

  Copyright (C) 2001-2017 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. - originator
                    http://www.pikron.com
            (C) 2003-2005 Frantisek Vacek <fanda.vacek@gmail.com>

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

// neccessary for getline()
#define _GNU_SOURCE

// pthread.h this should be first include (because of pthread_cancel())
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <malloc.h>
#include <string.h>

//#include <vca_hwmod.h>

#include <can_vca.h>
#include <ul_evcbase.h>
#include <ul_log.h>
#include <suiut/sui_event.h>
#include <suiut/sui_dinfo.h>
#include <suiut/sui_dievc.h>
#include <suiut/sui_dtreemem.h>

#include <pxmc_dinfo.h>

#include "appl_defs.h"
#include "appl_utils.h"

#define MODULE_ID "mz_apo_pmsm_rvapo_hwplugin"

//typedef void* working_thread_fn_t(void *param);

static pthread_t working_thread;

static sui_dtree_memnode_t memnode;
static sui_dtree_memdir_t dinfodir;

static int initialized = 0;

//======================== logging support ==========================

extern UL_LOG_CUST(ulogd_hwmodule);

//=================================================================

void
appl_stop(void)
{
  pxmc_done();
  ul_logerr("Application abnormal termination\n");
  sleep(1);
  /* stop clock pin driving FPGA to ensure failase state */
}

int
mz_apo_2dc_hwplugin_start(void)
{
  appl_setup_environment(MODULE_ID);

  pxmc_initialize();

  return 0;
}

//=================================================================

static
sui_dinfo_t *mz_apo_2dc_hwplugin_monitor_list[] = {
/*    &mz_apo_2dc_hwplugin_knob1_di,
    &mz_apo_2dc_hwplugin_knob2_di,
    &mz_apo_2dc_hwplugin_knob3_di,*/ /* Empty for now */
};

static const
int mz_apo_2dc_hwplugin_monitor_list_size =
  sizeof(mz_apo_2dc_hwplugin_monitor_list)/sizeof(*mz_apo_2dc_hwplugin_monitor_list);

static void *
working_thread_fn(void *param)
{
  unsigned long old_values[mz_apo_2dc_hwplugin_monitor_list_size];
  int first_run = 1;
  unsigned long sample_period_nsec = 100 * 1000 * 1000;
  struct timespec sample_period_time;
  int i;

  for (i = 0; i < mz_apo_2dc_hwplugin_monitor_list_size; i++)
    old_values[i] = 0;

  ul_logdeb("%s working thread IS RUNNING!\n", MODULE_ID);

  clock_gettime(CLOCK_MONOTONIC, &sample_period_time);

  while (1) {
    unsigned long val;
    sui_dinfo_t *datai;
    for (i = 0; i < mz_apo_2dc_hwplugin_monitor_list_size; i++) {
      datai = mz_apo_2dc_hwplugin_monitor_list[i];
      if (sui_rd_ulong(datai, 0, &val) == SUI_RET_OK) {
        if ((val != old_values[i]) && !first_run)
          sui_dinfo_changed(datai);
        old_values[i] = val;
      }
    }
    first_run = 0;

    sample_period_time.tv_nsec += sample_period_nsec;
    if (sample_period_time.tv_nsec > 1000*1000*1000) {
      sample_period_time.tv_nsec -= 1000*1000*1000;
      sample_period_time.tv_sec += 1;
    }
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &sample_period_time, NULL);
  }
  pthread_exit(0);
}

static int
start_working_thread()
{
  int ret;

  ul_logdeb("starting working thread\n");
  ret = pthread_create(&working_thread, NULL, working_thread_fn, (void *)NULL);
  if (ret) {
    ul_logerr("ERROR creating working thread, return code: %d\n", ret);
  }
  return ret;
}

static void
cancel_working_thread()
{
  ul_logdeb("waiting for working thread to terminate\n");
  pthread_cancel(working_thread);
  pthread_join(working_thread, NULL);
  ul_logdeb("working thread: JOINED\n");
}

//-------------------- exported symbols --------------------------

typedef struct module_dinfo_to_export_t {
  sui_dinfo_t *datai;
  char *name;
} module_dinfo_to_export_t;

module_dinfo_to_export_t module_dinfo_to_export[] = {
  {&pxmc_is_busy_di, "pxmc_is_busy"},
  {&pxmc_flg_di, "pxmc_flg"},
  {&pxmc_ap_di, "pxmc_ap"},
  {&pxmc_rp_di, "pxmc_rp"},
  {&pxmc_go_di, "pxmc_go"},
  {&pxmc_spd_di, "pxmc_spd"},
  {&pxmc_spdfg_di, "pxmc_spdfg"},
  {&pxmc_md_di, "pxmc_md"},
  {&pxmc_ms_di, "pxmc_ms"},
  {&pxmc_ma_di, "pxmc_ma"},
  {&pxmc_p_di, "pxmc_p"},
  {&pxmc_i_di, "pxmc_i"},
  {&pxmc_d_di, "pxmc_d"},
  {&pxmc_s1_di, "pxmc_s1"},
  {&pxmc_s2_di, "pxmc_s2"},
  {&pxmc_me_di, "pxmc_me"},
  {&pxmc_ene_di, "pxmc_ene"},
  {&pxmc_cfg_di, "pxmc_cfg"},
  {&pxmc_mode_di, "pxmc_mode"},
#ifdef PXMC_WITH_PHASE_TABLE
  {&pxmc_ptirc_di, "pxmc_ptirc"},
  {&pxmc_ptper_di, "pxmc_ptper"},
  {&pxmc_ptofs_di, "pxmc_ptofs"},
  {&pxmc_ptshift_di, "pxmc_ptshift"},
  {&pxmc_ptmark_di, "pxmc_ptmark"},
#endif /*PXMC_WITH_PHASE_TABLE*/
  {&pxmc_pwm1cor_di, "pxmc_pwm1cor"},
  {&pxmc_pwm2cor_di, "pxmc_pwm2cor"},

  {&pxmc_ap_sd_di, "pxmc_ap_sd"},
  {&pxmc_rp_sd_di, "pxmc_rp_sd"},
  {&pxmc_go_sd_di, "pxmc_go_sd"},
};

static const
int module_dinfo_to_export_size =
  sizeof(module_dinfo_to_export)/sizeof(*module_dinfo_to_export);

int
init_module(void)
{
  int ret = -1;
  int ok = 1;
  int i;

  ul_logdeb("PXMC HW module init\n");

  if (mz_apo_2dc_hwplugin_start() < 0) {
    ul_logerr("%s: *** ERROR *** mz_apo_2dc_hwplugin_start \n", __PRETTY_FUNCTION__);
    return -1;
  }

  // init root dir node
  sui_dtree_memdir_init(&dinfodir);
  // init dtreemem node
  sui_dtree_memnode_init(&memnode, MODULE_ID, SUI_DTREE_ISDIR);
  // embed root dir node to root memnode
  memnode.ptr.dir = &(dinfodir.dir);

  // fill dinfodir
  for (i = 0; i < module_dinfo_to_export_size; i++) {
    module_dinfo_to_export_t *dtoexp = &module_dinfo_to_export[i];
    sui_dtree_memnode_t *node;
    node = (sui_dtree_memnode_t *)malloc(sizeof(*node));
    if (node == NULL) {
      ul_logerr("%s: not enough memory\n", __PRETTY_FUNCTION__);
      ok = 0;
      break;
    }
    memset(node, 0, sizeof(*node));
    sui_dtree_memnode_init(node, dtoexp->name, SUI_DTREE_ISDINFO);
    node->ptr.datai = dtoexp->datai;
    if (sui_dtree_mem_insert(&dinfodir, node) <= 0) {
      ul_logerr("%s: *** ERROR *** inserting node '%s'\n", __PRETTY_FUNCTION__, node->name);
      ok = 0;
      break;
    }
  }

  initialized = 1;

  if (ok) {
    ret = start_working_thread();
  } else {
    ul_logerr("There were ERRORS dring module init. Working thread will not be started.\n");
  }

  return ret;
}

void
cleanup_module(void *clean_up)
{
  // decrementing refcnt frees dinfo, if it is not referenced by someone else
  ul_logdeb("HW module cleanup\n");
  cancel_working_thread();
  appl_stop();
}

sui_dtree_memnode_t *
get_dtreemem_node(void)
{
  if (!initialized)
    ul_logerr("%s(): ERROR module IS NOT initialized, call init_module() first.\n", __PRETTY_FUNCTION__);
  return &memnode;
}
//-------------------------------------------------------------
