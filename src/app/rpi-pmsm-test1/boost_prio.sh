#!/bin/sh

# ps Hxaww --sort rtprio -o pid,policy,rtprio,state,tname,time,command

SPI_PIDS=$(ps Hxa -o command,pid | \
  sed -n -e 's/^\[\(irq\/80-bcm2708_\|bcm2708_spi\.0\)\][ \t]*\([0-9]*\)$/\2/p')

for P in $SPI_PIDS ; do
  schedtool -F -p 95 $P
done
