#define CONFIGURE_INIT
#include <rtems/rtems/status.h>
#include "appl_defs.h"
#include "rtems_system.h"
#include <fcntl.h>
#include <rtems/error.h>
#include <rtems/monitor.h>
#include <rtems/shell.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>

#ifdef APPL_WITH_BSDNET
#include <machine/rtems-bsd-config.h>
#include <rtems/dhcpcd.h>

#define DEFAULT_NETWORK_DHCPCD_ENABLE

#if defined(DEFAULT_NETWORK_DHCPCD_ENABLE) &&                                  \
    !defined(DEFAULT_NETWORK_NO_STATIC_IFCONFIG)
#define DEFAULT_NETWORK_NO_STATIC_IFCONFIG
#endif

#include <sysexits.h>

#include <rtems/bsd/bsd.h>
#include <rtems/rtems_bsdnet.h>

#endif /*CONFIG_APP_CAN_TEST_WITH_BSDNET*/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define CONFIGURE_SHELL_COMMANDS_INIT
#define CONFIGURE_SHELL_COMMANDS_ALL
#define CONFIGURE_SHELL_MOUNT_MSDOS

sem_t sem1;

#include <bsp/irq-info.h>

#ifdef APPL_WITH_BSDNET

#include <rtems/netcmds-config.h>

#ifdef RTEMS_BSD_MODULE_USER_SPACE_WLANSTATS
#define SHELL_WLANSTATS_COMMAND &rtems_shell_WLANSTATS_Command,
#else
#define SHELL_WLANSTATS_COMMAND
#endif

#ifdef RTEMS_BSD_MODULE_USR_SBIN_WPA_SUPPLICANT
#define SHELL_WPA_SUPPLICANT_COMMAND &rtems_shell_WPA_SUPPLICANT_Command,
#else
#define SHELL_WPA_SUPPLICANT_COMMAND
#endif

#ifdef SHELL_TTCP_COMMAND_ENABLE
#define SHELL_TTCP_COMMAND &rtems_shell_TTCP_Command,
#else
#define SHELL_TTCP_COMMAND
#endif

#define CONFIGURE_SHELL_USER_COMMANDS                                          \
  SHELL_WLANSTATS_COMMAND                                                      \
  SHELL_WPA_SUPPLICANT_COMMAND                                                 \
  SHELL_TTCP_COMMAND                                                           \
  &bsp_interrupt_shell_command, &rtems_shell_ARP_Command,                      \
      &rtems_shell_HOSTNAME_Command, &rtems_shell_PING_Command,                \
      &rtems_shell_ROUTE_Command, &rtems_shell_NETSTAT_Command,                \
      &rtems_shell_IFCONFIG_Command, &rtems_shell_TCPDUMP_Command,             \
      &rtems_shell_SYSCTL_Command, &rtems_shell_VMSTAT_Command

#endif /*APPL_WITH_BSDNET*/

#include <rtems/shellconfig.h>

#define BUILD_VERSION_STRING(major, minor, patch)                              \
  __XSTRING(major) "." __XSTRING(minor) "." __XSTRING(patch)

void bad_rtems_status(rtems_status_code status, int fail_level,
                      const char *text) {
  printf("ERROR: %s status %s", text, rtems_status_text(status));
  status = rtems_task_delete(RTEMS_SELF);
}

static inline void check_rtems_status(rtems_status_code status, int fail_level,
                                      const char *text) {
  if (!rtems_is_status_successful(status))
    bad_rtems_status(status, fail_level, text);
}

int testcmd_forshell(int argc, char **argv) {
  int i;
  printf("Command %s called\n", argv[0]);
  for (i = 1; i < argc; i++)
    if (argv[i])
      printf("%s", argv[i]);
  printf("\n");
  return 0;
}

#ifdef APPL_WITH_BSDNET

static void default_network_dhcpcd(void) {
#ifdef DEFAULT_NETWORK_DHCPCD_ENABLE
  static const char default_cfg[] = "clientid libbsd test client\n";
  rtems_status_code sc;
  int fd;
  int rv;
  ssize_t n;

  fd =
      open("/etc/dhcpcd.conf", O_CREAT | O_WRONLY, S_IRWXU | S_IRWXG | S_IRWXO);
  assert(fd >= 0);

  n = write(fd, default_cfg, sizeof(default_cfg) - 1);
  assert(n == (ssize_t)sizeof(default_cfg) - 1);

#ifdef DEFAULT_NETWORK_DHCPCD_NO_DHCP_DISCOVERY
  static const char nodhcp_cfg[] = "nodhcp\nnodhcp6\n";

  n = write(fd, nodhcp_cfg, sizeof(nodhcp_cfg) - 1);
  assert(n == (ssize_t)sizeof(nodhcp_cfg) - 1);
#endif

  rv = close(fd);
  assert(rv == 0);

  sc = rtems_dhcpcd_start(NULL);
  assert(sc == RTEMS_SUCCESSFUL);
#endif
}

int network_init(void) {
  rtems_status_code sc;

  sc = rtems_bsd_initialize();
  if (sc != RTEMS_SUCCESSFUL) {
    printf("rtems_bsd_initialize failed\n");
    return -1;
  }

  /* Let the callout timer allocate its resources */

  sc = rtems_task_wake_after(2);
  if (sc != RTEMS_SUCCESSFUL) {
    printf("rtems_task_wake_after failed\n");
    return -1;
  }

  default_network_dhcpcd();

  return 0;
}

#endif /*APPL_WITH_BSDNET*/

rtems_task Init(rtems_task_argument ignored) {
  printf("\n\nRTEMS v " BUILD_VERSION_STRING(__RTEMS_MAJOR__, __RTEMS_MINOR__,
                                             __RTEMS_REVISION__) "\n");

#ifdef APPL_WITH_BSDNET
  if (network_init() == -1) {
    printf("rtems_bsd_ifconfig_lo0 failed\n");
  };
#endif /*APPL_WITH_BSDNET*/

  rtems_monitor_init(RTEMS_MONITOR_SUSPEND | RTEMS_MONITOR_GLOBAL);
  /*rtems_capture_cli_init (0);*/

  printf("Starting application\n");

  rtems_shell_init("SHLL", RTEMS_MINIMUM_STACK_SIZE + 0x1000,
                   SHELL_TASK_PRIORITY, "/dev/console", 1, 0, NULL);

  rtems_shell_add_cmd("main", "app", "main", main);

  rtems_task_delete(RTEMS_SELF);

  printf("*** END OF INIT TASK ***\n");
  exit(0);
}
