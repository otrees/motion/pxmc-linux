/* sui_dtrans.h
 *
 * Header file for DINFO transformation functions
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_DTRANS_H_
  #define _SUI_DTRANS_H_

#include "suiut/sui_dinfo.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * struct sui_lintrans_par - Long value linear transformation structure
 * @refcnt:	Reference counter
 * @multiply:	Multiplication factor
 * @divide:	Divide factor
 * @offset:	Value offset
 *
 * The value read through DINFO proxy is first multiplied, then divided
 * and offset is applied to the obtained value.
 *
 * @RDVal = ( @Raw * @multiply ) / @divide + @offset
 *
 * The value written through the proxy is transformed according next equation
 *
 * @Raw = ( ( @WRVal - @offset ) * @divide ) / @multiply
 *
 * File: sui_dtrans.h
 */
typedef struct sui_lintrans_par {
  int refcnt;
  long multiply;
  long divide;
  long offset;
} sui_lintrans_par_t;

void sui_lintrans_par_inc_refcnt(sui_lintrans_par_t *par);
void sui_lintrans_par_dec_refcnt(sui_lintrans_par_t *par);
int sui_lintrans_mul_div(long *res, long val, long mul, long div);
int sui_lintrans_mul_div_round(long *res, long val, long mul, long div);

int sui_lintrans_proxy_rdval(sui_dinfo_t *dinfo, long indx, void *buf);
int sui_lintrans_proxy_wrval(sui_dinfo_t *dinfo, long indx, const void *buf);
sui_dinfo_t *dinfo_lintrans_proxy( sui_dinfo_t *dfrom, int afdig, 
             long amin, long amax, sui_lintrans_par_t *trans_par, int options);

int sui_dinfo_copy_long_fdigits(sui_dinfo_t *dfrom, sui_dinfo_t *dto, int idx);

#define SUI_TRANS_OPT_LIMRD 0x10

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUI_DTRANS_H_*/

