/* sui_dtrans.c 
 *
 * DINFO transformation functions
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "ul_utmalloc.h"
#include "suiut/sui_dinfo.h"
#include "suiut/sui_event.h"
#include "sui_dtrans.h"
#include <stdlib.h>

#define CC_HAS_LONG_LONG

/**
 * sui_lintrans_par_inc_refcnt - Increase reference count of parameters
 * @par: Pointer to linear transformation parameters structure.
 *
 * File: sui_dtrans.c
 */
void sui_lintrans_par_inc_refcnt(sui_lintrans_par_t *par)
{
  if(!par) return;
  if( par->refcnt >= 0) par->refcnt++;
}

/**
 * sui_lintrans_par_dec_refcnt - Decrease reference count of parameters
 * @par: Pointer to linear transformation parameters structure.
 *
 * If the reference count reaches zero, the memory to store parameters
 * is released.
 *
 * File: sui_dinfo.c
 */
void sui_lintrans_par_dec_refcnt(sui_lintrans_par_t *par)
{
  if(!par) return;
  if(par->refcnt>0) par->refcnt--;
  if(!par->refcnt) {
    free(par);
  }
}

#ifdef CC_HAS_LONG_LONG

/**
 * sui_lintrans_mul_div_round - Long value combined multiply and divide with rounding
 * @res:	pointer to the place to store operation result
 * @val:	value to be transformed
 * @mul:	multiplier
 * @div:	divisor
 *
 * @res = round( ( @val * @mul ) / @div )
 *
 * Return Value: %SUI_RET_OK for successful proceeding of operation.
 *		%SUI_RET_ERR if divisor is zero. %SUI_RET_EOORP/%SUI_RET_EOORN
 *		if return value is too big/low to fit into long type range.
 * File: sui_dinfo.c
 */
int sui_lintrans_mul_div_round(long *res, long val, long mul, long div)
{
  long long ll;
  const unsigned long max=~0l;
  const unsigned long max2=max/2;
  if(!div) return SUI_RET_ERR;
  if(!mul || !val) {
    *res=0;
    return SUI_RET_OK;
  }
  ll=val;
  ll*=mul;
  if(ll>0)      ll+=labs(div)/2;
  else if(ll<0) ll-=labs(div)/2;
  ll/=div;
  if(ll>(long long)max2)
    return SUI_RET_EOORP;
  if(ll<-(long long)max2)
    return SUI_RET_EOORN;
  *res=ll;
  return SUI_RET_OK;
}

/**
 * sui_lintrans_mul_div - Long value combined multiply and divide
 * @res:	pointer to the place to store operation result
 * @val:	value to be transformed
 * @mul:	multiplier
 * @div:	divisor
 *
 * @res = ( @val * @mul ) / @div
 *
 * Return Value: %SUI_RET_OK for successful proceeding of operation.
 *		%SUI_RET_ERR if divisor is zero. %SUI_RET_EOORP/%SUI_RET_EOORN
 *		if return value is too big/low to fit into long type range.
 * File: sui_dinfo.c
 */
int sui_lintrans_mul_div(long *res, long val, long mul, long div)
{
  long long ll;
  const unsigned long max=~0l;
  const unsigned long max2=max/2;
  if(!div) return SUI_RET_ERR;
  if(!mul || !val) {
    *res=0;
    return SUI_RET_OK;
  }
  ll=val;
  ll*=mul;
  ll/=div;
  if(ll>(long long)max2)
    return SUI_RET_EOORP;
  if(ll<-(long long)max2)
    return SUI_RET_EOORN;
  *res=ll;
  return SUI_RET_OK;
}


#else /*CC_HAS_LONG_LONG*/

#define ul(p) ((unsigned long)p)

int sui_lintrans_mul_div(long *res, long val, long mul, long div)
{
  unsigned long rem;
  unsigned long max=~0l;
  unsigned long max2=max/2;
  unsigned long maxd;
  unsigned long umul;
  int sig;
  if(!div) return SUI_RET_ERR;
  if(!mul || !val) {
    *res=0;
    return SUI_RET_OK;
  }
  if(mul<0){sig=1; umul=-mul;}
  else { sig=0; umul=mul;}
  maxd=max2/umul;
  if((val<=(long)maxd) && (val>=-(long)maxd)){
    val*=mul;
    val/=div;
  } else {
    if(val<0) { val=-val; sig^=1; }
    if(div<0) { div=-div; sig^=1; }
    rem=ul(val)%ul(div);
    val=ul(val)/ul(div);
    if((unsigned long)val>=(unsigned long)maxd)
      return sig?SUI_RET_EOORN:SUI_RET_EOORP;
    val=ul(val)*umul+(rem*umul)/ul(div);
    if(sig)
      val=-val;
  }
  *res=val;
  return SUI_RET_OK;
}

#endif /*CC_HAS_LONG_LONG*/

int sui_lintrans_proxy_rdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
  int ret;
  long val;
  sui_dinfo_t *dfrom=(sui_dinfo_t*)(dinfo->ptr);
  sui_lintrans_par_t *par;
  if(!dfrom) return SUI_RET_NCON;
  if(!dfrom->rdval) return SUI_RET_ERR;
  ret=sui_rd_long(dfrom, indx, &val);
  if(ret!=SUI_RET_OK) return ret;

  par=(sui_lintrans_par_t*)dinfo->info;
  ret=sui_lintrans_mul_div(&val,val,par->multiply,par->divide);
  val+=par->offset;
  if(ret!=SUI_RET_OK) return ret;
  *(long*)buf=val;
  return SUI_RET_OK;
}

int sui_lintrans_proxy_limrdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
  int ret;
  long val;
  ret=sui_lintrans_proxy_rdval(dinfo,indx,&val);
  if(ret!=SUI_RET_OK) return ret;
  if(val>dinfo->maxval)
    val=dinfo->maxval;
  if(val<dinfo->minval)
    val=dinfo->minval;
  *(long*)buf=val;
  return SUI_RET_OK;
}

int sui_lintrans_proxy_wrval(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  int ret;
  long val;
  sui_dinfo_t *dfrom=(sui_dinfo_t*)(dinfo->ptr);
  sui_lintrans_par_t *par;
  if(!dfrom) return SUI_RET_NCON;
  if(!dfrom->wrval) return SUI_RET_ERR;
  val=*(long*)buf;

  par=(sui_lintrans_par_t*)dinfo->info;
  val-=par->offset;
  ret=sui_lintrans_mul_div(&val,val,par->divide,par->multiply);
  if(ret!=SUI_RET_OK) return ret;
  return sui_wr_long(dfrom, indx, &val);
}

void dinfo_lintrans_proxy_hevent(struct sui_dinfo *dinfo, struct sui_event *event)
{
  switch(event->what){
    case SUEV_COMMAND:
      if(event->message.command!=SUCM_DONE) break;
    case SUEV_FREE:
      if(dinfo->info)
        sui_lintrans_par_dec_refcnt((sui_lintrans_par_t*)dinfo->info);
      dinfo->info=0;
      break;
  }
  dinfo_simple_proxy_hevent(dinfo,event);
}

/**
 * dinfo_lintrans_proxy - Create new linear transformation proxy.
 * @dfrom:	Pointer to the underlying DINFO providing raw value
 * @afdig:	Number of fraction digits reported by created DINFO
 * @amin:	Minimal value reported by DINFO
 * @amax:	Maximal value reported by DINFO
 * @trans_par:	Pointer to the transformation parameters
 * @options:	Transformation options: %SUI_TRANS_OPT_LIMRD specifies limiting
 *		of read value according to @amin and @amax parameters range.
 *
 * Return Value: Pointer to the created proxy. %NULL indicates insufficient memory
 *		for proxy allocation.
 * File: sui_dinfo.c
 */
sui_dinfo_t *dinfo_lintrans_proxy( sui_dinfo_t *dfrom, int afdig, long amin, long amax,
                              sui_lintrans_par_t *trans_par, int options)
{
  sui_dinfo_t *dinfo;
  sui_datai_rdfnc_t *rdfnc;
  
  if(options&SUI_TRANS_OPT_LIMRD)
    rdfnc=sui_lintrans_proxy_limrdval;
  else
    rdfnc=sui_lintrans_proxy_rdval;

  dinfo=sui_create_dinfo( dfrom, afdig, amin, amax, (long)trans_par, rdfnc, sui_lintrans_proxy_wrval);
  if(dinfo==NULL) return NULL;
  sui_lintrans_par_inc_refcnt(trans_par);
  dinfo->hevent=dinfo_lintrans_proxy_hevent;
  dinfo->evmask=(short)(SUEV_COMMAND | SUEV_FREE);
  return dinfo;
}

/**
 * sui_dinfo_copy_long_fdigits - copy long value from one dinfo to other with respect to fdigits
 * @dfrom: pointer to an input dinfo
 * @dto: pointer to an output dinfo
 */
int sui_dinfo_copy_long_fdigits(sui_dinfo_t *dfrom, sui_dinfo_t *dto, int idx)
{
  #define MAXMULTIPLIER 6

  const long multip[MAXMULTIPLIER+1] = {
          1,
         10,
        100,
       1000,
      10000,
     100000,
    1000000,
  };

  long mul = 1, div = 1, val = 0;
  int df = 0, ret;

  if (!dfrom || !dto) return SUI_RET_ERR;
  df = dfrom->fdigits - dto->fdigits;
  if (df<-MAXMULTIPLIER || df>MAXMULTIPLIER) return SUI_RET_ERR;
  if (df>0)
    div = multip[df];
  else
    mul = multip[-df];
  ret = sui_rd_long(dfrom, idx, &val);
  if (ret!=SUI_RET_OK) return ret;
  ret = sui_lintrans_mul_div_round(&val, val, mul, div);
  if (ret!=SUI_RET_OK) return ret;
  ret = sui_wr_long(dto, idx, &val);
  return ret;
}

