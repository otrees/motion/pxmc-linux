#include <string.h>
#include <ul_utdefs.h>
#include "sui_dtree.h"

//sui_dtree_dir_t *sui_dtree_global_root;

/**
 * sui_dtree_lookup - Find dinfo in the named dinfo database
 * @from_dir: the directory to start from
 * @path:   path from directory to dinfo or directory
 * @found_dir: the optional pointer to space that would hold pointer to directory of found dinfo
 * @datai:  optional pointer to store the found dinfo
 *
 * Return Value: SUI_DTREE_FOUND, SUI_DTREE_DIR, SUI_DTREE_NOPATH, SUI_DTREE_ERROR
 * File: sui_dtree.c
 */

 int sui_dtree_lookup(sui_dtree_dir_t *from_dir, const char *path, sui_dtree_dir_t **found_dir,
                     sui_dinfo_t **datai)
 {
     sui_dtree_dir_t *dir;
     int res;
     int consumed;
     
     const char *p=path;
     if(from_dir==NULL) {
         return SUI_DTREE_ERROR;
         //from_dir=sui_dtree_global_root;
     }
     if(found_dir!=NULL) *found_dir = NULL;
     if(datai!=NULL) *datai = NULL;
     dir=from_dir;
     if(!dir) return SUI_DTREE_NOPATH;
     
     while(1){
         while(*p=='/') p++;
         if(!*p) return SUI_DTREE_NOPATH;
         if(!dir) return SUI_DTREE_ERROR;
         if(!dir->lookup) return SUI_DTREE_ERROR;
         res=dir->lookup(dir, p, &consumed, &dir, datai);
         if(res==SUI_DTREE_FOUND) {
             if(found_dir!=NULL) *found_dir = dir;
             return res;
         }
         if(res==SUI_DTREE_ERROR) return res;
         if(res==SUI_DTREE_NOPATH) return res;
         //if(res==SUI_DTREE_DIR) return SUI_DTREE_ERROR;
         
         p+=consumed;
     }
     if(found_dir!=NULL) *found_dir = dir;
     return SUI_DTREE_DIR;
 }
