/* Definitions of IOs */
#include <unistd.h> 
#include <cmd_proc.h>
#include <stdio.h>

static int std_putc(cmd_io_t *cmd_io, int ch) { 
    int r=putchar(ch);
    fflush(stdout);
    return r;
}

//int _getkey_nb(void);
static int std_getc(cmd_io_t *cmd_io) { 
    return getchar();           /* On UNIX, we don't use non-blocking
                                 * variant. */
    //return _getkey_nb();
}
static int std_write(cmd_io_t *cmd_io, const void *buf, int count) {
    return write(1, buf, count);
}
static int std_read(cmd_io_t *cmd_io, void *buf, int count) {
    return read(0, buf, count); 
}

cmd_io_t cmd_io_std={
    putc:std_putc,
    getc:std_getc,
    write:std_write,
    read:std_read
};

